This repository contains the project I had to complete for my semester in Czestochowa.

This version only supports noise based on randomness by normal law, not JPEG compression (as my last attempts failed to train).

The most important scripts are noise_images.py and denoise_images.py.
The file paths are hardcoded, sorry about that, so it might be really hard to even run the scripts on your own.

The .h5 are the neural network models. The most recent one is denoise_images.h5.

Files related to denoising numbers where used as training before denoising images. They aren't used anymore.
