import os
import shutil
import random
import glob

import numpy as np
from tensorflow.keras.preprocessing import image

import matplotlib
import matplotlib.pyplot as plt
import tkinter
matplotlib.use("TkAgg")

def copy_random(source, dest, n):
	os.chdir(source)
	names = random.sample(glob.glob("*"), n)
	for index, name in enumerate(names):
		print(f"Copy {index}/{n}")
		shutil.copyfile(os.path.abspath(name), dest + f"/{index}.jpeg")

def copy_noised(source, dest):
	os.chdir(source)
	names = glob.glob("*")
	src_paths = [os.path.abspath(name) for name in names]
	os.chdir(dest)
	dst_paths = [os.path.abspath(name) for name in names]
	for index, (src_path, dst_path) in enumerate(zip(src_paths, dst_paths)):
		print(f"Noised: {index}")
		img = load_image(src_path)
		noised_img = noise_image(img)
		plt.imsave(dst_path, noised_img)
	
	
def load_image(path):
	img = image.load_img(path)
	img_array = image.img_to_array(img)
	# expanded_array = np.expand_dims(img_array, axis=0)
	normalized_array = img_array.astype("float32") / 255.0
	return normalized_array

def noise_image(img):
	noise_factor = 0.7
	noisy_array = img + noise_factor * np.random.normal(
		loc=0.0, scale=1.0, size=img.shape
	)

	return np.clip(noisy_array, 0.0, 1.0)


def display(array1, array2):
	"""
	Displays ten random images from each one of the supplied arrays
	"""
	n = 1

	indices = np.random.randint(len(array1), size=n)
	images1 = array1[indices, :]
	images2 = array2[indices, :]

	plt.figure()
	for i, (image1, image2) in enumerate(zip(images1, images2)):
		ax = plt.subplot(2, n, i + 1)
		plt.imshow(image1)
		ax.get_xaxis().set_visible(False)
		ax.get_yaxis().set_visible(False)

		ax = plt.subplot(2, n, i + 1 + n)
		plt.imshow(image2)
		ax.get_xaxis().set_visible(False)
		ax.get_yaxis().set_visible(False)
	
	plt.show()

copy_random(
	"/media/velimir/EMTEC X210/NoisyImages/old_test",
	"/media/velimir/EMTEC X210/NoisyImages/test_normal",
	30_100
)
copy_noised(
	"/media/velimir/EMTEC X210/NoisyImages/test_normal",
	"/media/velimir/EMTEC X210/NoisyImages/test_noised"
)
