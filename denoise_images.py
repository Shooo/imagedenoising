import os
import shutil
import glob
import random

import numpy as np
import tensorflow as tf
from tensorflow.keras.preprocessing import image
import matplotlib
import matplotlib.pyplot as plt
import tkinter
matplotlib.use("TkAgg")

from tensorflow.keras import layers
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Model


def load_image(path):
	img = image.load_img(path)
	img_array = image.img_to_array(img)
	# expanded_array = np.expand_dims(img_array, axis=0)
	normalized_array = img_array.astype("float32") / 255.0
	# crop it to a multiple of 8
	cropped_array = normalized_array[:normalized_array.shape[0]//8*8, :normalized_array.shape[1]//8*8, :]
	return cropped_array

def import_names(path, n):
	os.chdir(path)
	names = random.sample(glob.glob("*"), n)
	return names

def import_data(path, names):
	os.chdir(path)
	images = np.array([
		image
		for image in (
			load_image(os.path.abspath(name))
			for name in names
		)
		if image.shape[0] < 1000 and image.shape[1] < 1000
	], dtype=object)
	return images


def display(array1, array2):
	"""
	Displays ten random images from each one of the supplied arrays
	"""
	n = 10

	indices = np.random.randint(len(array1), size=n)
	images1 = array1[indices]
	images2 = array2[indices]

	plt.figure(figsize=(20, 4))
	for i, (image1, image2) in enumerate(zip(images1, images2)):
		ax = plt.subplot(2, n, i + 1)
		plt.imshow(image1)
		ax.get_xaxis().set_visible(False)
		ax.get_yaxis().set_visible(False)

		ax = plt.subplot(2, n, i + 1 + n)
		plt.imshow(image2)
		ax.get_xaxis().set_visible(False)
		ax.get_yaxis().set_visible(False)
	
	plt.show()

def create_autoencoder():
	input = layers.Input(shape=(None, None, 3))

	# Encoder
	x = layers.Conv2D(32, (3, 3), activation="relu", padding="same")(input)
	x = layers.MaxPooling2D((2, 2), padding="same")(x)
	x = layers.Conv2D(32, (3, 3), activation="relu", padding="same")(x)
	x = layers.MaxPooling2D((2, 2), padding="same")(x)
	x = layers.Conv2D(32, (3, 3), activation="relu", padding="same")(x)
	x = layers.MaxPooling2D((2, 2), padding="same")(x)

	# Decoder
	x = layers.Conv2DTranspose(32, (3, 3), strides=2, activation="relu", padding="same")(x)
	x = layers.Conv2DTranspose(32, (3, 3), strides=2, activation="relu", padding="same")(x)
	x = layers.Conv2DTranspose(32, (3, 3), strides=2, activation="relu", padding="same")(x)
	x = layers.Conv2D(3, (3, 3), activation="sigmoid", padding="same")(x)

	# Autoencoder
	autoencoder = Model(input, x)
	autoencoder.compile(optimizer="adam", loss="binary_crossentropy")
	autoencoder.summary()
	return autoencoder

def train_autoencoder(autoencoder, input_data, target_data):
	# because we have variable image sizes, I can't train by batch
	for input, target in zip(input_data, target_data):
		input = np.expand_dims(input, axis=0)
		target = np.expand_dims(target, axis=0)
		autoencoder.fit(
			x=input,
			y=target,
			epochs=1,
			batch_size=1,
			validation_data=(input, target)
		)
	
def predict(autoencoder, input_data):
	res = []
	for input_image in input_data:
		output_image = autoencoder.predict(np.expand_dims(input_image, axis=0))[0,:,:,:]
		print(f"Shape: {output_image.shape}")
		res.append(output_image)
	return np.array(res)

if __name__ == "__main__":
	training_ammount = 30_000
	testing_ammount = 100
	names = import_names(
		"/media/velimir/EMTEC X210/NoisyImages/test_normal",
		training_ammount + testing_ammount
	)
	random.shuffle(names)
	train_names = names[:training_ammount]
	test_names = names[training_ammount:]

	os.chdir("/home/velimir/Travail/2A/autoencoder/")
	if os.path.isfile("denoise_images.h5") is False:
		print("Created autoencoder")
		autoencoder = create_autoencoder()

		print("\n___VANILLA TRAINING___")
		for i in range(0, training_ammount, 100):
			print(f"Vanilla batch {i}->{i+100}")
			current_train_names = train_names[i:i+100]
			train_data = import_data(
				"/media/velimir/EMTEC X210/NoisyImages/test_normal",
				current_train_names
			)
			print(f"Amount after filter: {len(train_data)}")
			train_autoencoder(autoencoder, train_data, train_data)

		print("\n___NOISY TRAINING___")
		for i in range(0, training_ammount, 100):
			print(f"Noisy batch {i}->{i+100}")
			current_train_names = train_names[i:i+100]
			train_data = import_data(
				"/media/velimir/EMTEC X210/NoisyImages/test_normal",
				current_train_names
			)
			print(f"Amount after filter: {len(train_data)}")
			noisy_train_data = import_data(
				"/media/velimir/EMTEC X210/NoisyImages/test_noised",
				current_train_names
			)
			train_autoencoder(autoencoder, noisy_train_data, train_data)

		os.chdir("/home/velimir/Travail/2A/autoencoder/")
		autoencoder.save("denoise_images.h5")
	else:
		from tensorflow.keras.models import load_model
		print("Loaded previous autoencoder")
		autoencoder = load_model("denoise_images.h5")
		autoencoder.summary()

	test_data = import_data(
		"/media/velimir/EMTEC X210/NoisyImages/test_normal",
		test_names
	)
	noisy_test_data = import_data(
		"/media/velimir/EMTEC X210/NoisyImages/test_noised",
		test_names
	)

	predictions = predict(autoencoder, test_data)
	display(test_data, predictions)

	predictions = predict(autoencoder, noisy_test_data)
	display(noisy_test_data, predictions)
